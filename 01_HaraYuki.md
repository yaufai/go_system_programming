<!-- slide -->
# Goならわかるシステムプログラミング
Hara　Yuki

<!-- slide -->
# Topics
- Introduction
	- What is "system programming"?
	- Development environment
- System call
	- Implementation of "Hello world"
	- What is system call?
	- Roles of system call
	- from golang side
	- from OS side
- interface


<!-- slide -->
# Goで覗くシステムプログラミングの世界
<!-- slide -->
# システムプログラミングとは？
人によっていろいろな定義がありうるが。。。
## OSの提供する機能を使ったプログラミング
の意味でこのweb記事は書いている。

- メモリ管理
- プロセス管理
- プロセス間の通信
- ファイルシステム
- ネットワーク
- ユーザ管理
- タイマー
<!-- slide -->

# Goとは？
> Go言語は、C言語の性能とPythonの書きやすさ／読みやすさを両立させ、 モダンな言語の特徴をうまく取り入れた言語となることを目標にGoogleが開発したプログラミング言語です


- Go言語は多くのOSの機能を直接扱えて、なおかつ少ない行数で動くアプリケーションが作れる
-  Go言語はC/C++よりもコードを書き始める前のライブラリの収集が簡単
- C/C++のように道を踏み外して地雷が爆発するようなことも、ガベージコレクションのおかげでメモリ管理を手動で頑張る必要もない
- コンパイルも早く、エラーメッセージもわかりやすくなっています

※　個人差があります

<!-- slide -->
# 余談：GoはCにとってかわるか？

筆者個人の意見としてはNo
- 作られるバイナリは長くなる
- Cに性能面で劣る

cf. [Which language has the brightest future in replacement of C between D, Go and Rust? And Why?](https://www.quora.com/Which-language-has-the-brightest-future-in-replacement-of-C-between-D-Go-and-Rust-And-Why/answer/Andrei-Alexandrescu?srid=T7AW)

<!-- slide -->
# Goのセットアップ
- Go
- Git
- IntelliJ系のIDE

http://ascii.jp/elem/000/001/234/1234843/#fn3

デバッガを使って`fmt.Println("Hello world!")`で何が起こっているのかをみていきます。

<s>連載記事を参考に各自おねがいします。。</s>
最新のintellijで互換しなくなったようです。

<!-- slide -->
# 書籍版では

Visual Studioを使っているらしい。

<!-- slide -->
# My environment
- Golang 1.9
- delve (debugger)
- go-debug (atom extension)

`sudo apt-get install golang-go`ではgolang 1.6.2。これではdelveが使えないので注意。

<!-- slide -->
# なにはともあれ、golang自体とステップイン機能（後述）をもつgoのデバッガがあればいい。

<!-- slide -->
# Introduction to SYSTEM CALL
<!-- slide -->
# Hello World!
```
package main
import "fmt"
func main() {
    fmt.Println("Hello World!")
}
```

このHello Worldを実行した時に何が起こってるか？
`fmt.Println()`の実装を追って、システムコールを導入します。

<!-- slide -->
# Hello World!
```go
package main
import "fmt"
func main() {
    fmt.Println("Hello World!")
}
```

`Println`にブレークポイントをおいて実行。ステップイン（その関数の定義をみる）で実装を見ると？

<!-- slide -->
# Implementation of `fmt.Println()`

```go
// Println formats using the default formats for its operands and writes to standard output.
// Spaces are always added between operands and a newline is appended.
// It returns the number of bytes written and any write error encountered.
func Println(a ...interface{}) (n int, err error) {
	return Fprintln(os.Stdout, a...)
}
```

複数の引数をとる`Fprintln()`に`os.Stdout`を渡して`Fprintln()`を使いやすくする便利関数。

<!-- slide -->

# Implementation of `Fprintln()`
```go
// Fprintln formats using the default formats for its operands and writes to w.
// Spaces are always added between operands and a newline is appended.
// It returns the number of bytes written and any write error encountered.
func Fprintln(w io.Writer, a ...interface{}) (n int, err error) {
	p := newPrinter()
	p.doPrintln(a)
	n, err = w.Write(p.buf)
	p.free()
	return
}
```
<!-- slide -->
# Implementation of `io.Writer.Write()`
```go
func (f *File) Write(b []byte) (n int, err error) {
	if err := f.checkValid("write"); err != nil {
		return 0, err
	}
	n, e := f.write(b)
	if n < 0 {
		n = 0
	}
	if n != len(b) {
		err = io.ErrShortWrite
	}

	epipecheck(f, e)

	if e != nil {
		err = f.wrapErr("write", e)
	}

	return n, err
}
```
`*File.write()`は小文字で始まるので非公開関数。

<!-- slide -->
# Implementation of `*File.write()`
- 実装はOSによる。

Linux, mac
```go
// write writes len(b) bytes to the File.
// It returns the number of bytes written and an error, if any.
func (f *File) write(b []byte) (n int, err error) {
	n, err = f.pfd.Write(b)
	runtime.KeepAlive(f)
	return n, err
}
```

<!-- slide -->


```go
// Write implements io.Writer.
func (fd *FD) Write(p []byte) (int, error) {
	if err := fd.writeLock(); err != nil {
		return 0, err
	}
	defer fd.writeUnlock()
	if err := fd.pd.prepareWrite(fd.isFile); err != nil {
		return 0, err
	}
	var nn int
	for {
		max := len(p)
		if fd.IsStream && max-nn > maxRW {
			max = nn + maxRW
		}
		n, err := syscall.Write(fd.Sysfd, p[nn:max])
		if n > 0 {
			nn += n
		}
		if nn == len(p) {
			return nn, err
		}
		if err == syscall.EAGAIN && fd.pd.pollable() {
			if err = fd.pd.waitWrite(fd.isFile); err == nil {
				continue
			}
		}
		if err != nil {
			return nn, err
		}
		if n == 0 {
			return nn, io.ErrUnexpectedEOF
		}
	}
}

```

ここでシステムコールを呼んでいる。
```go
n, err := syscall.Write(fd.Sysfd, p[nn:max])
```
<!-- slide -->
# システムコールとは？

<!-- slide -->
# CPUのモード
あるプロセスが他のプロセスのメモリ空間に作用できるのは非常に危険。
- プロセスがバグっているかもしれない。
- 悪意のあるプロセスが他のプロセスを書き換えるかもしれない。

動作モードに応じて権限を設定する。
(e.g.) Intel CPUには４つのモードがある。（wikipediaより）


よく使われるのは、
- 特権モード　（OSが動作する）
- ユーザモード (おおくのアプリケーションが動作する)

ファイルの読み書き、インターネットの通信など普段のアプリケーションで当たり前に使っている機能がユーザモードではできないようになっている！

<!-- slide -->
# システムコールとは？
特権モードでの処理を通常のアプリで実行できる仕組み。

CPUメーカによって当然実装はマチマチだが、基本的には
1. （通常の関数呼び出しのように、）OSの提供する関数を呼び出す
2. OSが特権モードで呼ばれた関数を実行

<!-- slide -->
# システムコール VS 関数呼び出し
- システムコールでは呼び出せる関数の数に制限（１個）
- 呼び出し元の認証が必要
- 全ての命令がシステムコールに提供している訳ではない

「関数呼び出し」というのはたとえだと思ってください。



<!-- slide -->
# システムコールの果たす役割
特権モードがなくても、プロセスは多くのCPU命令にはアクセスできる。

ただ、その操作はどうしてもそのプロセスの範囲だけ。（他のプロセスのメモリには操作できない）

- 計算自体はプロセスの中で完結するので計算はできる。
- でも、その結果を外部に出力できない

外部への出力：
- ファイル操作で出力
- インターネット通信で送信
- 共有メモリに置く
<!-- slide -->


# システムコールがなかったら
入力を受け取ってメモリを確保することも、プロセス作成・終了もできないのでプロセスが始まることができない。

プロセスが始まっていたとしても、計算結果を使って外部に作用できない。

> プロセスにできることは電力を消費して熱を発生させるぐらい

(by author)



<!-- slide -->
# Goにおけるシステムコールの実装
`os.File`構造体は４つのインターフェイスを満たす
- `io.Reader`
- `io.Writer`
- `io.Seeker`
- `io.Closer`

それぞれで`syscall`パッケージにある関数を呼び出す

<!-- slide -->
| システムコール関数	| 機能 |
| :-------------- | :-- |
| func syscall.Open(path string, mode int, perm uint32) |	ファイルを開く（作成も含む）|
| func syscall.Read(fd int, p []byte)	| ファイルから読み込みを行う |
| func syscall.Write(fd int, p []byte)	| ファイルに書き込みを行う |
| func syscall.Close(fd int) |	ファイルを閉じる |
| func syscall.Seek(fd int, offset int64, whence int) |	ファイルの書き込み／読み込み位置を移動する |

この`syscall`の実装はOSによって異なる。

<!-- slide -->
# OSからみたシステムコール

<!-- slide -->
# POSIX
POSIX : Portable Operating System Interface

システムコールを呼び出すためのCの関数名と引数と返り値を定める。

実際に標準Cに含まれるのはPOSIXで定義した関数ではなく、さらにそれをラップした関数。

(e.g.)
POSIX : `open()`, `write()`, .....
ISO C : `fopen()`, `fwrite()`, .....

ラップしてnon-POSIX OS (windows)でも同じようなプログラミングができるようになったり、便利に使えるようインターフェイスを改善したりしている。

<!-- slide -->
<!-- TODO:５−２の続き -->

<!-- slide -->
# goのインターフェイス

<!-- slide -->
# Do you Remember?
`fmt.Println()`から`syscall.Write()`までたどる時に`*File.Write()`という関数がありました。
```go
func (f *File) Write(b []byte) (n int, err error) {
	if err := f.checkValid("write"); err != nil {
		return 0, err
	}
	n, e := f.write(b)
	if n < 0 {
		n = 0
	}
	if n != len(b) {
		err = io.ErrShortWrite
	}

	epipecheck(f, e)

	if e != nil {
		err = f.wrapErr("write", e)
	}

	return n, err
}
```

<!-- slide -->
この`*File.Write()`の役割は、
「byte列を受け取って、`*File`に書き込む。byte列の長さnを返す。書き込めなければerrorもつける。」

この機能を機能を様々な場面で活用したい！…よね？

通常の意味でのファイル以外にも、通信でもデバイスでもファイルとしてみなす（Linux特有の考え方？）ことができる。

<!-- slide -->
余談：
「なんでもファイルとみなす」という考え方はUNIX哲学の一つ、と以前何かで読んだ気がするのですが改めて「unix哲学」ググっても、

「データはテキストファイル入れる」

は出てきても、「デバイス」「通信」をファイルとして処理するかまで明言している文献が見当たらない。。


Linuxで調べるとそれらしい記述が見つかることが多いけど、これはLinuxの考えですか？
それともUnix哲学のこのスローガンはここまで含意している？？

もし"The UNIX Philosophy" by Mike Gancarsを読んだ方がみえましたら盲信します。

<!-- slide -->
# io.Write interface
```go
type Writer interface {
    Write(p []byte) (n int, err error)
}
```

Javaはクラスをまるっと抽象化するが、goではメソッドを抽象化。

ある構造体$S$がインターフェイス$I$を実装したメソッドを持つ時、「$S$は$I$のインターフェイスをもつ」という。
<!-- slide -->
# 実装の例

<!-- slide -->
# Example #1 ファイル出力

`*File.Write()`

<!-- slide -->
# Example #2 画面出力

`os.Stdout.Write()`

```go
func Println(a ...interface{}) (n int, err error) {
	return Fprintln(os.Stdout, a...)
}

func Fprintln(w io.Writer, a ...interface{}) (n int, err error) {
	p := newPrinter()
	p.doPrintln(a)
	n, err = w.Write(p.buf)
	p.free()
	return
}
```
`Println()`から`Fprintln()`を呼ぶと`w`は`os.Stdout`。


<!-- slide -->
# Example #3 バッファに書き込む
`bytes.Buffer.Write()`
```go
func main() {
    var buffer bytes.Buffer
    buffer.Write([]byte("bytes.Buffer example\n"))
    fmt.Println(buffer.String())
}
```

情報をどんどんためていって最後に一気に受け取るのに使うらしい。
文字列に足していくことの方が自分はよくやるけど、この方がメモリ消費がいいのかな？

<!-- slide -->
# Example #4 HTTP通信
`http.ResponseWriter.Write()`
```go
func handler(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("http.ResponseWriter sample"))
}
 
func main() {
    http.HandleFunc("/", handler)
    http.ListenAndServe(":8080", nil)
}
```
See localhost:8080.

<!-- slide -->
# Example #5 一旦圧縮して他のWriteに渡す。
```go
func main() {
    file, err := os.Create("test.txt.gz")
    if err != nil {
        panic(err)
    }
    writer := gzip.NewWriter(file)
    writer.Header.Name = "test.txt"
    writer.Write([]byte("gzip.Writer example\n"))
    writer.Close()
}
```
他にもハッシュ計算を行ってから渡す、などがある。
