<!-- slide -->
# 輪読会の進め方

<!-- slide -->
# What to Read
[Goならわかるシステムプログラミング](http://ascii.jp/elem/000/001/235/1235262/)


内容としてはOSの提供する機能を使ったプログラミングをgolangを題材に学習していく。

みんなで癒されよう。

<!-- slide -->
# 担当の決め方
In total there are 20 chapters in the reading:
- Introduction
- IO(Reader)
- ネットワーク（TCP通信）
- ネットワーク（その他）
- ファイルシステム
- プロセス
- 並列処理
- メモリ管理、コンテナ

We have 9 members and 7 remaining chapters. Each chapter is demanding, so 2 or 3 people will be assigned to each.

Since this is not university curriculum, no one should not be forced.... Volunteers are really appreciated.
<!-- slide -->
# Introduction

- 第1回   Goで覗くシステムプログラミングの世界
- 第2回   低レベルアクセスへの入り口（1）：io.Writer
- 第5回   Goから見たシステムコール

DONE.

<!-- slide -->
# IO(Reader)

- 第3回   低レベルアクセスへの入り口（2）：io.Reader前編
- 第4回   低レベルアクセスへの入り口（3）：io.Reader後編

<!-- slide -->
# ネットワーク（TCP通信）

- 第6回   GoでたたくTCPソケット（前編）
- 第7回   GoでたたくTCPソケット（後編）

<!-- slide -->
# ネットワーク（その他）

- 第8回   UDPソケットをGoで叩く
- 第9回   Unixドメインソケット

<!-- slide -->
# ファイルシステム
- 第10回   ファイルシステムと、その上のGo言語の関数たち（1）
- 第11回   ファイルシステムと、その上のGo言語の関数たち（2）
- 第12回   ファイルシステムと、その上のGo言語の関数たち（3）

<!-- slide -->
# プロセス
- 第13回   Go言語で知るプロセス（1）
- 第14回   Go言語で知るプロセス（2）
- 第15回   Go言語で知るプロセス（3）

<!-- slide -->
# 並列処理
- 第16回   Go言語と並列処理
- 第17回   Go言語と並列処理（2）
- 第18回   Go言語と並列処理（3）

<!-- slide -->
# メモリ管理、コンテナ

- 第19回   Go言語のメモリ管理
- 第20回   Go言語とコンテナ

<!-- slide -->
# Assignment #1
Find one or more sessions you want to present.


<!-- slide -->
# Frequency?

- Once a week?
- Try complete one session with one gathering.
- Then, 2 months can be enough to complete....


TOO RUSH, MAYBE?
<!-- slide -->
# Assignment #2
Vote your best weekdays for this seminar.


<!-- slide -->
# How To Manage Our Materials
I guess most of you love **git** for this purpose, ...don't you?

- github public branch (everyone has an account)
- bitbucket private branch (safe)


<!-- slide -->
# Naming

```
{session_number}_{name}.{extension}
```

(e.g.) 01_HaraYuki.html, 01_HaraYuki.ppt

<!-- slide -->
# Cares for environmental differences
Include a stable copy of your material.

If you create markdown presentation,
make sure that your final commit includes an HTML output.

If you create PowerPoint presentation,
make sure that your final commit includes a PDF output.

<!-- slide -->
# Directory Structure

One idea:

```
.
├── Seminar management stuffs (.gitignore, AboutSeminar.html,)
├── environmentally-free materials
├── raw materials
└── resources
    ├── 01 (Stores referred items like codes, img,...)
    └── 02
```

<!-- slide -->
# Summary
Assignments:
- find your topicS
- find your day
